package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.constant.DateConst;
import ru.fadeev.tm.util.DateUtil;

import java.lang.Exception;
import java.util.List;
import java.util.UUID;

@Category(IntegrationTest.class)
public class TaskEndpointTest extends AbstractTest {

    @Test
    public void persistTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("testUser");
        taskEndpoint.persistTask(tokenUser, task);
        Assert.assertNotNull(taskEndpoint.findIdByNameTask(tokenUser, "testUser"));
    }

    @Test
    public void findAllTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenAdmin, task3);
        List<TaskDTO> tasks = taskEndpoint.findAllTask(tokenUser);
        Assert.assertEquals(2, tasks.size());
        List<TaskDTO> adminTasks = taskEndpoint.findAllTask(tokenAdmin);
        Assert.assertEquals(1, adminTasks.size());
    }

    @Test
    public void removeAllTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenAdmin, task3);
        taskEndpoint.removeAllTask(tokenUser);
        taskEndpoint.removeAllTask(tokenAdmin);
        List<TaskDTO> tasks = taskEndpoint.findAllTask(tokenUser);
        Assert.assertEquals(0, tasks.size());
        List<TaskDTO> adminTasks = taskEndpoint.findAllTask(tokenAdmin);
        Assert.assertEquals(0, adminTasks.size());
    }

    @Test
    public void findIdByNameTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull final TaskDTO testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals(task.getName(), testTask.getName());
        Assert.assertNull(taskEndpoint.findOneTask(tokenAdmin, "test"));
    }

    @Test
    public void findOneTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull final TaskDTO testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals(task.getName(), testTask.getName());
        Assert.assertNull(taskEndpoint.findOneTask(tokenAdmin, "test"));
    }

    @Test
    public void mergeTaskTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull TaskDTO testTask = taskEndpoint.findOneTask(tokenUser, id);
        testTask.setDescription("test description");
        taskEndpoint.mergeTask(tokenUser, testTask);
        testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals("test description",testTask.getDescription());
        testTask.setDescription("admin test");
        taskEndpoint.mergeTask(tokenAdmin, testTask);
        testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals("test description", testTask.getDescription());
    }

    @Test
    public void searchByDescriptionTaskTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        task.setName("test1");
        task2.setName("test2");
        task3.setName("test3");
        task.setDescription("ale");
        task2.setDescription("pale");
        task3.setDescription("vova");
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.searchByDescriptionTask(tokenUser, "ale");
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO testTask : tasks)
            Assert.assertTrue(
                    testTask.getDescription().equals("ale") ||
                            testTask.getDescription().equals("pale")
            );
    }

    @Test
    public void searchByNameTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        task.setName("ale");
        task2.setName("pale");
        task3.setName("vova");
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.searchByNameTask(tokenUser, "ale");
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO testTasks : tasks)
            Assert.assertTrue(
                    testTasks.getName().equals("ale") ||
                            testTasks.getName().equals("pale")
            );
    }

    @Test
    public void removeTaskTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull String taskId = taskEndpoint.findIdByNameTask(tokenUser,"test");
        taskEndpoint.removeTask(tokenAdmin,taskId);
        Assert.assertEquals("test", taskEndpoint.findOneTask(tokenUser, taskId).getName());
        taskEndpoint.removeTask(tokenUser,taskId);
        Assert.assertNull(taskEndpoint.findOneTask(tokenUser, taskId));

    }

    @Test
    public void removeAllByProjectIdTaskTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        @NotNull final String testProjectId = UUID.randomUUID().toString();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setId(testProjectId);
        task.setProjectId(testProjectId);
        task2.setProjectId(testProjectId);
        projectEndpoint.persistProject(tokenUser,project);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.removeAllByProjectIdTask(tokenAdmin, testProjectId);
        Assert.assertEquals(3,taskEndpoint.findAllTask(tokenUser).size());
        taskEndpoint.removeAllByProjectIdTask(tokenUser,testProjectId);
        Assert.assertEquals(1,taskEndpoint.findAllTask(tokenUser).size());
    }

    @Test
    public void removeAllByProjectTaskTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        @NotNull TaskDTO task4 = new TaskDTO();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        @NotNull ProjectDTO project2 = new ProjectDTO();
        project2.setId(UUID.randomUUID().toString());
        task.setProjectId(project.getId());
        task2.setProjectId(project.getId());
        task3.setProjectId(project2.getId());
        projectEndpoint.persistProject(tokenUser,project);
        projectEndpoint.persistProject(tokenUser,project2);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.persistTask(tokenUser,task4);
        taskEndpoint.removeAllProjectTask(tokenUser);
        Assert.assertEquals(1,taskEndpoint.findAllTask(tokenUser).size());
    }

    @Test
    public void findAllByTaskIdTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        @NotNull TaskDTO task4 = new TaskDTO();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        @NotNull ProjectDTO project2 = new ProjectDTO();
        project2.setId(UUID.randomUUID().toString());
        task.setProjectId(project.getId());
        task2.setProjectId(project.getId());
        task3.setProjectId(project2.getId());
        projectEndpoint.persistProject(tokenUser,project);
        projectEndpoint.persistProject(tokenUser,project2);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.persistTask(tokenUser,task4);
        List<TaskDTO> tasks = taskEndpoint.findAllByProjectIdTask(tokenUser,project.getId());
        Assert.assertEquals(2,tasks.size());
        List<TaskDTO> tasksAdmin = taskEndpoint.findAllByProjectIdTask(tokenAdmin,project.getId());
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByStatusTest() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        task.setStatus(Status.IN_PROGRESS);
        task2.setStatus(Status.DONE);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.sortByStatusTask(tokenUser);
        Assert.assertEquals(Status.DONE,tasks.get(0).getStatus());
        Assert.assertEquals(Status.PLANNED,tasks.get(2).getStatus());
        List<TaskDTO> tasksAdmin = taskEndpoint.sortByStatusTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByStartDateTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        task.setName("Task");
        task2.setName("Task2");
        task3.setName("Task3");
        task.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        task2.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        task3.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.sortByStartDateTask(tokenUser);
        Assert.assertEquals("Task2",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(2).getName());
        List<TaskDTO> tasksAdmin = taskEndpoint.sortByStartDateTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByFinishDateTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        task.setName("Task");
        task2.setName("Task2");
        task3.setName("Task3");
        task.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        task2.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        task3.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.sortByFinishDateTask(tokenUser);
        Assert.assertEquals("Task2",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(1).getName());
        List<TaskDTO> tasksAdmin = taskEndpoint.sortByFinishDateTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByCreationTest() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("Task");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull TaskDTO task2 = new TaskDTO();
        task2.setName("Task2");
        Thread.sleep(1000);
        taskEndpoint.persistTask(tokenUser, task2);
        @NotNull TaskDTO task3 = new TaskDTO();
        task3.setName("Task3");
        Thread.sleep(1000);
        taskEndpoint.persistTask(tokenUser, task3);
        List<TaskDTO> tasks = taskEndpoint.sortByCreationTimeTask(tokenUser);
        Assert.assertEquals("Task",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(2).getName());
        List<TaskDTO> tasksAdmin = taskEndpoint.sortByCreationTimeTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

}
