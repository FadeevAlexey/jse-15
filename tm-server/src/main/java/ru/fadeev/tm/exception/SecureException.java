package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class SecureException extends RuntimeException {

    public SecureException() {
    }

    public SecureException(@Nullable final String message) {
        super(message);
    }

}
