package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;

public final class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}